#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QSpinBox>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->mainToolBar->setVisible(false);

    udpSender = new QUdpSocket(this);
    dataManager = new DataManager(this);
    textEditChanged();
    ledtAddressChanged();
    sbxPortChanged(ui->sbxPort->value());

    connect(ui->textEdit, &QTextEdit::textChanged, this, &MainWindow::textEditChanged);
    connect(ui->ledtAddress, &QLineEdit::textChanged, this, &MainWindow::ledtAddressChanged);
    connect(ui->sbxPort, static_cast<void(QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            this, &MainWindow::sbxPortChanged);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::actionOpenTriggled);
    connect(ui->btnSend, &QPushButton::clicked, this, &MainWindow::btnSendClicked);
    connect(ui->btnPause, &QPushButton::clicked, this, &MainWindow::btnPauseClicked);
    connect(ui->btnStop, &QPushButton::clicked, this, &MainWindow::btnStopClicked);
    connect(udpSender, &QUdpSocket::connected, this, &MainWindow::udpSenderConnected);
    connect(this, &MainWindow::startDataManager, dataManager, &DataManager::startManage);
    connect(this, &MainWindow::pauseDataManager, dataManager, &DataManager::puaseManage);
    connect(this, &MainWindow::stopDataManager, dataManager, &DataManager::resetManage);
    connect(dataManager, &DataManager::timeToSend, this, &MainWindow::udpSendData);
    connect(dataManager, &DataManager::sendOver, this, &MainWindow::dataSendOver);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete udpSender;
    delete dataManager;
}

void MainWindow::textEditChanged()
{
    text = ui->textEdit->toPlainText();
    updateState();
}

void MainWindow::ledtAddressChanged()
{
    hostAddress = QHostAddress(ui->ledtAddress->text());
    updateState();
}

void MainWindow::sbxPortChanged(int port)
{
    this->port = port;
    updateState();
}

void MainWindow::actionOpenTriggled()
{
    if(openFile())
    {
        updateTextEdit();
    }
}

/**
 * @brief MainWindow::btnSendClicked
 */
void MainWindow::btnSendClicked()
{
    qDebug()<<"btnSendClicked";
    if(currentState==Pause)
    {
        setState(Sending);
        emit startDataManager();
    }
    else
    {
        setState(Busy);
        dataManager->setText(text);
        udpSender->connectToHost(hostAddress, port, QIODevice::WriteOnly);
        //
    }
}

/**
 * @brief MainWindow::btnPauseClicked
 */
void MainWindow::btnPauseClicked()
{
    qDebug()<<"btnPauseClicked";
    emit pauseDataManager();
    setState(Pause);
}

/**
 * @brief MainWindow::btnStopClicked
 */
void MainWindow::btnStopClicked()
{
    qDebug()<<"btnStopClicked";
    emit stopDataManager();
    udpSender->disconnectFromHost();
    updateState();
}

/**
 * @brief MainWindow::udpSenderConnected
 */
void MainWindow::udpSenderConnected()
{
    setState(Sending);
    emit startDataManager();
}

void MainWindow::udpSendData(QByteArray data)
{
    qDebug()<<"udpSendData";
    udpSender->writeDatagram(data.data(), data.size(), hostAddress, port);
}

void MainWindow::dataSendOver()
{
    qDebug()<<"dataSendOver";
    udpSender->disconnectFromHost();
    updateState();
}

bool MainWindow::openFile()
{
    QString fileName;
    fileName = QFileDialog::getOpenFileName(this,
          QString("Open Text"), NULL, QString("Text Files (*.txt)"));
    if(fileName.isEmpty())
        return false;

    QFile file(fileName);
    if(file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
        QTextStream textStream(&file);
        text = textStream.readAll();
        return true;
    }
    else
    {
        QMessageBox::information(NULL, NULL, "Open file error.");
        return false;
    }
}

/**
 * @brief MainWindow::updateState
 */
void MainWindow::updateState()
{
    if(text.isEmpty()||hostAddress.isNull())
    {
        setState(NotReady);
    }
    else
    {
        setState(Ready);
    }
}

/**
 * @brief MainWindow::setState
 * @param state
 */
void MainWindow::setState(MainWindow::State state)
{
    currentState = state;
    switch (state) {
    case NotReady:
        ui->actionOpen->setDisabled(false);
        ui->textEdit->setReadOnly(false);
        ui->ledtAddress->setReadOnly(false);
        ui->sbxPort->setReadOnly(false);
        ui->btnSend->setDisabled(true);
        ui->btnPause->setDisabled(true);
        ui->btnStop->setDisabled(true);
        ui->statusBar->showMessage("NotReady");
        break;
    case Ready:
        ui->actionOpen->setDisabled(false);
        ui->textEdit->setReadOnly(false);
        ui->ledtAddress->setReadOnly(false);
        ui->sbxPort->setReadOnly(false);
        ui->btnSend->setDisabled(false);
        ui->btnPause->setDisabled(true);
        ui->btnStop->setDisabled(true);
        ui->statusBar->showMessage("Ready");
        break;
    case Busy:
        ui->actionOpen->setDisabled(true);
        ui->textEdit->setReadOnly(true);
        ui->ledtAddress->setReadOnly(true);
        ui->sbxPort->setReadOnly(true);
        ui->btnSend->setDisabled(true);
        ui->btnPause->setDisabled(true);
        ui->btnStop->setDisabled(true);
        ui->statusBar->showMessage("Busy");
        break;
    case Sending:
        ui->actionOpen->setDisabled(true);
        ui->textEdit->setReadOnly(true);
        ui->ledtAddress->setReadOnly(true);
        ui->sbxPort->setReadOnly(true);
        ui->btnSend->setDisabled(true);
        ui->btnPause->setDisabled(false);
        ui->btnStop->setDisabled(false);
        ui->statusBar->showMessage("Sending");
        break;
    case Pause:
        ui->actionOpen->setDisabled(true);
        ui->textEdit->setReadOnly(true);
        ui->ledtAddress->setReadOnly(true);
        ui->sbxPort->setReadOnly(true);
        ui->btnSend->setDisabled(false);
        ui->btnPause->setDisabled(true);
        ui->btnStop->setDisabled(false);
        ui->statusBar->showMessage("Pause");
        break;
    default:
        break;
    }
}

void MainWindow::updateTextEdit()
{
    ui->textEdit->setText(text);
}
