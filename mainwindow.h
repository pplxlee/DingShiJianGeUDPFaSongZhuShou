#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUdpSocket>
#include "datamanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void startDataManager();
    void pauseDataManager();
    void stopDataManager();

public slots:
    void textEditChanged();
    void ledtAddressChanged();
    void sbxPortChanged(int port);
    void actionOpenTriggled();
    void btnSendClicked();
    void btnPauseClicked();
    void btnStopClicked();
    void udpSenderConnected();
    void udpSendData(QByteArray data);
    void dataSendOver();

private:
    Ui::MainWindow *ui;
    QString text;
    QHostAddress hostAddress;
    quint16 port;
    typedef enum State{
        NotReady,
        Ready,
        Busy,
        Sending,
        Pause,
    }State;
    State currentState;
    QUdpSocket *udpSender;
    DataManager *dataManager;

private:
    bool openFile();
    void updateState();
    void setState(State state);
    void updateTextEdit();
};

#endif // MAINWINDOW_H
