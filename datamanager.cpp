#include "datamanager.h"

DataManager::DataManager(QObject *parent) : QObject(parent)
{
    resetManage();
}

void DataManager::timerEvent(QTimerEvent *event)
{
    int elapsedTime = time.elapsed();
    if(elapsedTime + lastElapsedTime >= timeAndDatas.first().time)
    {
        time.start();
        lastElapsedTime = elapsedTime + lastElapsedTime - timeAndDatas.first().time;
        TimeAndData temp = timeAndDatas.dequeue();
        emit timeToSend(temp.data);
    }
    if(timeAndDatas.isEmpty())
    {
        resetManage();
        emit sendOver();
    }
}

void DataManager::startManage()
{
    if(timeAndDatas.isEmpty())
    {
        emit timeToSend(text);
        emit sendOver();
        return;
    }
    timerID = startTimer(10);
    time.start();
}

void DataManager::puaseManage()
{
    killTimer(timerID);
    int elapsedTime = time.elapsed();
    lastElapsedTime += elapsedTime;
}

void DataManager::resetManage()
{
    killTimer(timerID);
    lastElapsedTime = 0;
    timeAndDatas.clear();
}

void DataManager::setText(QString text)
{
    this->text = text.toUtf8();
    text2TimeAndDatas();
}

void DataManager::text2TimeAndDatas()
{
    int i = 0, j = 0;
    QString timeStr = QString("time:");
    QString dataStr = QString("data:");
    while(((i = text.indexOf(timeStr, j)) != -1)
          && (j = (text.indexOf(dataStr, i))) != -1)
    {
        int indexOfTime = i+timeStr.size();
        int lenOfTime = j-indexOfTime;
        int indexOfData = j+dataStr.size();
        j = text.indexOf(timeStr, indexOfData);
        if(j == -1)
        {
            j = text.size();
        }
        int lenOfData = j - indexOfData;
        TimeAndData temp;
        temp.time = text.mid(indexOfTime, lenOfTime).trimmed().toInt();
        temp.data = text.mid(indexOfData, lenOfData).trimmed();
        timeAndDatas.enqueue(temp);
    }
}
