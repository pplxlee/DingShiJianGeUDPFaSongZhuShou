#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>
#include <QTime>
#include <QTimerEvent>
#include <QQueue>

class DataManager : public QObject
{
    Q_OBJECT
public:
    explicit DataManager(QObject *parent = nullptr);

protected:
    void timerEvent(QTimerEvent *event);

private:
    int timerID;
    QByteArray text;
    QTime time;
    int lastElapsedTime;

    typedef struct TimeAndData{
        int time;
        QByteArray data;
    }TimeAndData;
    QQueue<TimeAndData> timeAndDatas;

signals:
    void timeToSend(QByteArray);
    void sendOver();

public slots:
    void startManage();
    void puaseManage();
    void resetManage();

public:
    void setText(QString text);

private:
    void text2TimeAndDatas();
};

#endif // DATAMANAGER_H
